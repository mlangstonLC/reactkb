# ReactKB

Small demo of Redux state management for interactive applications

Framework and POC for future HTML5 typing tutor

Should work in any modern browser, including mobile

# Running

` npm install `

` npm start `

Service should run by default on localhost:3000
