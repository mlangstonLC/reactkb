import React, { Component } from 'react';

class ScreenBtn extends Component{
	
	constructor(props){
		super(props);
		this.state = {
			text: props.text,
			evtHandler: props.evtHandler
		}
	}

	handle = () => {
		this.state.evtHandler();
	}

	render(){
		return(
			<button id="goFS" onClick={this.handle}>{this.state.text}</button>
		);
	}
}

class ScreenControl extends Component{

	constructor(props){
		super(props)
		this.doFS = this.doFS.bind(this);
		this.quitFS = this.quitFS.bind(this);
		this.hijackEsc = this.hijackEsc.bind(this);
		this.screenfull = require('screenfull')
	}

	//can fullscreen a specific element, or the "document element"
	doFS() {
	  if(this.screenfull.enabled){
	  	this.screenfull.request();
	  }
	}

	//exit fullscreen is always done on the document itself
	quitFS() {
	  if(this.screenfull.enabled){
	  	this.screenfull.exit();
	  }
	}

	hijackEsc() {
	  this.doFS();
	}

	render(){
		return(
			<div>
				<ScreenBtn evtHandler={this.doFS} text="Go fullscreen" />
				<ScreenBtn evtHandler={this.quitFS} text="Exit fullscreen" />
			</div>
		);
	}	
}

export default ScreenControl;