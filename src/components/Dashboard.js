import { connect } from 'react-redux'
import React, { Component } from 'react'; 


class Dashboard extends Component{

    clear = () => {
        this.props.dispatch({type: 'CLEAR_STATE'})
    }

    render(){
        return(
                <button type="button" onClick={ this.clear}>
                    {"Clear"}
                </button>
        )
    }
}

//TODO: mapStateToProps, below is bad practice
var blah = connect(state => state)(Dashboard)
export default blah 