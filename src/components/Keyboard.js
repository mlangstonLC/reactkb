/* eslint-disable */

import React, { Component } from 'react'; 
import { connect } from 'react-redux'
import '../resources/index.css';

const Key = ({activated,label,clicked}) => 
		<button type="button" 
				className={['key', activated].join(' ')}
				onClick={clicked} >
			{label}
		</button>

class Keyboard extends Component{

	componentDidMount(){
		document.addEventListener("keydown", (evt) => this.handleKey(evt.key))
		this.cancelQueue = {}
	}

	handleKey = (letter) => {
		//TODO: for fun, remove
		if(letter === "Meta"){
			for(let i = 0; i < 1000; i++){
				let letters = Object.keys(this.props._keys)
				let theLetter = letters[Math.floor(Math.random()*letters.length)]

				setTimeout(() => {
					//start and end 
					this.props.dispatch({type: 'START_ACTION',letter: theLetter ,meta: {delay: 80}})
					this.props.dispatch({type: 'END_ACTION',letter: theLetter,meta: {delay: 1500}})
				}, 100+i)
			}
		}

		//throttle
		let threshold = 500//ms
		let timeMS = new Date().getTime()
		let meta = this.props.meta
		let oldTimeMS = (this.props.meta && this.props.meta.timestamp) 
			? keyTime : undefined		

		letter = letter.toUpperCase()
		//if already activated, cancel and end immediately - then continue to restart
		if(this.props._keys[letter]){
			if(this.cancelQueue[letter]) this.cancelQueue[letter]();
			//small delay to let "animation end" take effect
			this.props.dispatch({type: 'END_ACTION',letter: letter,meta: {delay: 1}})	
		}
		//start and end 
		this.props.dispatch({type: 'START_ACTION',letter: letter,meta: {delay: 80}})
		this.cancelQueue = {[letter]: 
			this.props.dispatch({type: 'END_ACTION',letter: letter,meta: {delay: 1500}})
		}	
	}

	render(){
		let keyCmps = Object.keys(this.props._keys).map(aKey => {
			return <Key label={aKey}
						activated={this.props._keys[aKey]} 
						key={aKey} id={"key"+aKey} 
						clicked={() => this.handleKey(aKey)} />
		})
		return(
			<div id="KB">
				{keyCmps}
			</div>
		)
	}
}

//TODO: mapStateToProps, below is bad practice
var connectedKeyboard = connect(state => {
	return {
		_keys: state.letterState._keys,
		meta: state.meta
	}	
})(Keyboard)

export default connectedKeyboard