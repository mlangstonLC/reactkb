import React from 'react';
import ScreenControl from './ScreenControl'
import Keyboard from './Keyboard'
import '../resources/App.css';

var App = () => {
	return(
		  <div className="App">
		    <ScreenControl />
		    <Keyboard />
		  </div>
	)
}

export default App;