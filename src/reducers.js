
var initialState = {
    _keys: {},
    meta: {
        delay: 0,
        timestamp: new Date().getTime()
    }
}
{
    ['1','2','3','4','5','6','7','8','9','0','-',
    '=','q','w','e','r','t','y','u','i','o','p','[',']',
    '\\','a','s','d','f','g','h','j','k','l',';','\'','z',
    'x','c','v','b','n','m',',','.','/','`']
    .map(s => s.toUpperCase()) //lazy!!
    .forEach(key => initialState._keys[key] = false)
}

//deep copy, no state mutation
var flipLetter = (letter,choice,state) => {
    return {
        ...state,
        _keys: {
            ...state._keys,
            [letter]: choice
        }
    }
}

export function letterState(state = initialState, action) {
    switch(action.type){
        case 'START_ACTION':
            return flipLetter(action.letter, true, state)
        case 'END_ACTION':
            return flipLetter(action.letter, false, state)
        default:
            return state;
    }
}