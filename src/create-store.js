import { createStore, applyMiddleware, combineReducers } from 'redux'
import * as reducers from './reducers'

//middleware
const timeoutScheduler = store => next => action => {
  if (!action.meta || !action.meta.delay) {
    return next(action)
  }

  let timeoutId = setTimeout(
    () => next(action),
    action.meta.delay
  )

  return function cancel() {
    console.log("Cancelled!")
    clearTimeout(timeoutId)
  }
}

const actionLogger = store => next => action => {
  if(action.type) console.log(Date.now()+": Sending action: "+action.type)
  return next(action)
}
// eslint-disable-next-line
const logger = store => next => action => {
  console.group(action.type)
  console.info('dispatching', action)
  let result = next(action)
  console.log('next state', store.getState())
  console.groupEnd(action.type)
  return result
}

export default function(data) {
  var reducer = combineReducers(reducers)
  var finalCreateStore = applyMiddleware(timeoutScheduler)(createStore)

  return finalCreateStore(reducer, data)
}
